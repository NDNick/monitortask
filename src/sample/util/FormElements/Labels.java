package sample.util.FormElements;

import javafx.scene.control.Label;

public class Labels {
    public Label sliderGenerateLabel;
    public Label sliderAnalysisLabel;



    public Labels() {
        sliderGenerateLabel = initLabel("Generate time", 50,85);
        sliderAnalysisLabel = initLabel("Analysis time", 50, 40);
    }

    private Label initLabel(String text, double layoutX, double layoutY) {
        Label label = new Label(text);
        label.setLayoutX(layoutX);
        label.setLayoutY(layoutY);
        return label;
    }
}
