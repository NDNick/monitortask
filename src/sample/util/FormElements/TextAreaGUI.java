package sample.util.FormElements;

import javafx.scene.control.TextArea;

public class TextAreaGUI {

    public TextArea textArea;

    public TextAreaGUI(){
        textArea = new TextArea();
        textArea.setEditable(false);
        textArea.setLayoutX(160);
        textArea.setLayoutY(30);
        double height = 400;
        double width = 300;
        textArea.setPrefHeight(height);
        textArea.setPrefWidth(width);
    }
}
