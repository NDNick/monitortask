package sample.util.FormElements;

import javafx.scene.control.Slider;
import sample.util.Data;

public class Sliders {

    public Slider sliderGenerate;
    public Slider sliderAnalysis;

    public Sliders(){
        sliderGenerate = initSlider(10,100);
        sliderAnalysis = initSlider(10,60);
        sliderGenerate.valueProperty().addListener((observable, oldValue, newValue) -> Data.getInstance().setGenerateTimeCount(newValue.intValue()));
        sliderAnalysis.valueProperty().addListener((observable, oldValue, newValue) -> Data.getInstance().setAnalysisTimeCount(newValue.intValue()));
    }
    private Slider initSlider(double layoutX, int layoutY) {
        Slider slider = new Slider(4, 10, 2);
        slider.setLayoutX(layoutX);
        slider.setLayoutY(layoutY);
        slider.setShowTickMarks(true);
        slider.setMajorTickUnit(1f);
        slider.setBlockIncrement(1f);
        return slider;
    }
}
