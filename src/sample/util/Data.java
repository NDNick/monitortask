package sample.util;

/**
 * Created by nick on 11/28/17.
 */
public class Data {

    private static volatile Data instance;

    String string = "";
    private int generateTimeCount = 4;
    private int analysisTimeCount = 4;
    private boolean action = false;

    public static Data getInstance(){
        Data localInstance = instance;
        if (localInstance == null){
            synchronized (Data.class){
                localInstance = instance;
                if (localInstance == null){
                    instance = localInstance = new Data();
                }
            }
        }
        return localInstance;
    }

    public int getGenerateTimeCount() {
        return this.generateTimeCount;
    }

    public void setAnalysisTimeCount(int analysisTimeCount) {
        this.analysisTimeCount = analysisTimeCount;
    }

    public int getAnalysisTimeCount() {
        return this.analysisTimeCount;
    }

    public void setGenerateTimeCount(int generateTimeCount) {
        this.generateTimeCount = generateTimeCount;
    }

    public void setString(String string) {
        this.string = string;
    }

    public String getString() {
        return this.string;
    }

    public void setAction(boolean action) {
        this.action = action;
    }

    public boolean getAction(){
        return this.action;
    }
}
