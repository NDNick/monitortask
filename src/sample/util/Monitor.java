package sample.util;

/**
 * Created by nick on 11/28/17.
 */
public class Monitor implements Runnable {

    Thread thread;
    Function function;

    public Monitor(String name, Function func){
        thread = new Thread(this, name);
        function = func;
        thread.start();
    }

    public void run() {
        if (thread.getName().compareTo("Generator") == 0){
            while (true){
                if (Data.getInstance().getAction()){
                    function.generateString(true);
                }
                else {
                    function.generateString(false);
                    break;
                }
            }
        }
        else {
            while (true) {
                if (Data.getInstance().getAction()) {
                    function.stringAnalysis(true);
                } else {
                    function.stringAnalysis(false);
                    break;
                }
            }
        }
    }
}

