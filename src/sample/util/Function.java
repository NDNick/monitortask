package sample.util;

import com.sun.corba.se.impl.orbutil.concurrent.Mutex;
import javafx.application.Platform;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static java.lang.Thread.sleep;

public class Function {

    private List<String> stringList = new ArrayList<>();
    private UpdateGUI updateGUI;

    public Function(UpdateGUI updateGUI){
        this.updateGUI = updateGUI;
    }//обновление интерфейса

    public void setStringList(List<String> stringList) {
        this.stringList = stringList;
    }

    public List<String> getStringList() {
        return stringList;
    }//список строк

    private Mutex mutex = new Mutex();//значение состояния

    public void generateString(Boolean run) {
        if (!run){
            mutex.release();
            return;
        }

        try {
            mutex.acquire();//блокировка входа
            String str = getGenerateString();//генерация строки
            Data.getInstance().setString(str);
            addStringToContainer(str);//добавление строки в контейнер
            System.out.println("generate: " + str);
            Platform.runLater(() -> updateGUI.updateTextArea());//обновление интерфейса
            mutex.release();//освобождение ресурса
            sleep(5000 / Data.getInstance().getGenerateTimeCount());
        } catch (InterruptedException ignored) {

        }
    }

    private String getGenerateString() {
        String str = "";
        char[] randChar = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'k',
                'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'x',
                'y', 'z','1', '2', '3', '4', '5', '6', '7', '8', '9', '0'};
        int strLength = new Random().nextInt(10) + 5;
        for (int i = 0; i < strLength; i++) {
            char a;
            a = randChar[new Random().nextInt(randChar.length)];
            str = str + String.valueOf(a);
        }
        return str;
    }

    private void addStringToContainer(String str)  {
        stringList.add(str);
        setStringList(stringList);
    }

    private int strIndex = 0;

    public void stringAnalysis(Boolean run)    {
        if (!run){
            mutex.release();
            return;
        }

        try {
            mutex.acquire();//захват ресурса
            if (stringList.size() == strIndex)
            {
                mutex.release();//если в списке нет не проверенных строк, освобождение ресурса
                return;
            }
            String string = stringList.get(strIndex);//получение строки из контейнера
            int countNumbers = getCountNumbers(string);//подсчет количества цифр в строке
            System.out.println("analysis: " + string);
            if (isEven(countNumbers)) {
                modifyString(string, strIndex);//замена цифр на звездочки
            }
            strIndex++;
            mutex.release();//освобождение ресурса
            sleep(5000 / Data.getInstance().getAnalysisTimeCount());
        } catch (InterruptedException ignored) {

        }
    }

    private int getCountNumbers(String string) {
        char[] charTmp = string.toCharArray();
        int iterator = 0;
        int countNumbers = 0;
        while (true){
            if (iterator == string.length())
            {
                break;
            }
            try {
                Integer.parseInt(String.valueOf(charTmp[iterator]));
                countNumbers++;
            } catch (NumberFormatException ignored) {}
            iterator++;
        }
        return countNumbers;
    }

    private boolean isEven(int number) {
        return number % 2 == 0;
    }

    private void modifyString(String string, int index){
        char[] a = string.toCharArray();
        for (int i = 0; i < string.length(); i++){
            try{
                Integer.parseInt(String.valueOf(a[i]));
                a[i] = '*';
            } catch (NumberFormatException ignored){}
        }
        string = String.valueOf(a);//получение модифицированного массива символов и перевод его в строку
        System.out.println("Modify: " + string);
        stringList.set(index, string);//установка модифицированной строки вместо исходной
        setStringList(stringList);
        Platform.runLater(() -> updateGUI.updateTextArea());//обновление интерфейса
    }
}

