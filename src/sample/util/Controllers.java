package sample.util;

import javafx.scene.Group;
import javafx.scene.control.*;
import javafx.scene.text.Font;
import sample.util.FormElements.Labels;
import sample.util.FormElements.Sliders;
import sample.util.FormElements.TextAreaGUI;

import java.io.FileNotFoundException;
import java.util.List;

/**
 * Created by nick on 11/24/17.
 */
public class Controllers implements UpdateGUI {

    TextAreaGUI textAreaGUI;

    public void initControllers(Group root) throws FileNotFoundException {
        Button btn = new Button("Start");
        setButtonAttributes(btn);
        setAction(btn);
        Sliders sliders = new Sliders();
        Labels labels = new Labels();
        textAreaGUI = new TextAreaGUI();
        root.getChildren().addAll(btn, sliders.sliderAnalysis, sliders.sliderGenerate,
                labels.sliderAnalysisLabel, labels.sliderGenerateLabel, textAreaGUI.textArea);
    }

    private void setButtonAttributes(Button button)   {
        button.setLayoutX(10);
        button.setLayoutY(10);
        button.setFont(Font.getDefault());
    }

    private void setAction(Button btn){
        btn.setOnAction(event -> switchAction(btn));
    }

    Function fc = new Function(this);
    Monitor generator;
    Monitor analyzer;

    private void switchAction(Button btn){
        if (btn.getText() == "Start")
        {
            btn.setText("Stop");
            Data.getInstance().setAction(true);
            generator = new Monitor("Generator", fc);
            analyzer = new Monitor("Analyzer", fc);
        }
        else
        {
            Data.getInstance().setAction(false);
            btn.setText("Start");
            generator.thread.interrupt();
            analyzer.thread.interrupt();
        }
    }

    @Override
    public void updateTextArea(){
        List<String> list = fc.getStringList();
        StringBuffer listText = new StringBuffer();
        for(String s : list){
            listText.append(s+"\n");
        }
        textAreaGUI.textArea.setText(String.valueOf(listText));
    }
}
